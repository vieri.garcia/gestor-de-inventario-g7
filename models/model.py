class Person:
    def __init__(self, name, lastname):
        self.name=name
        self.lastname=lastname
    
    def eat(self):
        print("Estoy comiendo")
    
    def sleep(self):
        print("Estoy durmiendo")
    
    def say_hello(self):
        print(f"Holi, me llamo {self.name} {self.lastname} ")
        
class Child(Person):
    def __init__(self, name, lastname):
        super().__init__(name,lastname)
    
    
# p1=Person("Christopher","Garcia")
# p1.say_hello()
# p1.sleep()
p2=Child("Cael", "Huaman")
p2.eat()