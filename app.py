from PySide2.QtWidgets import QApplication
from controllers.main_controller import AppWindowController

if __name__=="__main__":
    app=QApplication()
    window = AppWindowController()
    window.show()
    app.exec_()

