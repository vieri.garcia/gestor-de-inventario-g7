from PySide2.QtWidgets import QMainWindow
from views.main_ui import AppWindown

class AppWindowController(QMainWindow, AppWindown):
    
    def __init__(self):
        super().__init__()
        self.setupUi(self)
    
    def create_product(self):
        pass
    def delete_product(self):
        pass
    def populate_table(self):
        pass
    def get_search_criterias(self):
        pass
    def search_products(self):
        pass