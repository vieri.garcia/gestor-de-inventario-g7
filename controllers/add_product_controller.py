from PySide2.QtWidgets import QMainWindow
from views.add_product_ui import AddProductWindow

class AddProductWindowController(QMainWindow, AddProductWindow):
    
    def __init__(self):
        super().__init__()
        self.setupUi(self)
    
    def check_inputs(self):
        pass
    def create_product(self):
        pass
    def clean_inputs(self):
        pass