# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'add.product.window.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class AddProductWindow(object):
    def setupUi(self, AddProductWindow):
        if not AddProductWindow.objectName():
            AddProductWindow.setObjectName(u"AddProductWindow")
        AddProductWindow.resize(303, 365)
        self.WindowTitle = QLabel(AddProductWindow)
        self.WindowTitle.setObjectName(u"WindowTitle")
        self.WindowTitle.setGeometry(QRect(0, 20, 301, 31))
        font = QFont()
        font.setPointSize(11)
        font.setBold(True)
        font.setWeight(75)
        self.WindowTitle.setFont(font)
        self.WindowTitle.setAlignment(Qt.AlignCenter)
        self.NameLabel = QLabel(AddProductWindow)
        self.NameLabel.setObjectName(u"NameLabel")
        self.NameLabel.setGeometry(QRect(20, 60, 47, 13))
        self.NameInput = QLineEdit(AddProductWindow)
        self.NameInput.setObjectName(u"NameInput")
        self.NameInput.setGeometry(QRect(20, 80, 261, 20))
        self.DescriptionLabel = QLabel(AddProductWindow)
        self.DescriptionLabel.setObjectName(u"DescriptionLabel")
        self.DescriptionLabel.setGeometry(QRect(20, 110, 71, 16))
        self.DescriptionInput = QTextEdit(AddProductWindow)
        self.DescriptionInput.setObjectName(u"DescriptionInput")
        self.DescriptionInput.setGeometry(QRect(20, 130, 261, 71))
        self.PriceLabel = QLabel(AddProductWindow)
        self.PriceLabel.setObjectName(u"PriceLabel")
        self.PriceLabel.setGeometry(QRect(20, 210, 47, 13))
        self.PriceInput = QLineEdit(AddProductWindow)
        self.PriceInput.setObjectName(u"PriceInput")
        self.PriceInput.setGeometry(QRect(20, 230, 120, 20))
        self.DateLabel = QLabel(AddProductWindow)
        self.DateLabel.setObjectName(u"DateLabel")
        self.DateLabel.setGeometry(QRect(160, 210, 91, 16))
        self.DateInput = QDateEdit(AddProductWindow)
        self.DateInput.setObjectName(u"DateInput")
        self.DateInput.setGeometry(QRect(160, 230, 120, 22))
        self.StockLabel = QLabel(AddProductWindow)
        self.StockLabel.setObjectName(u"StockLabel")
        self.StockLabel.setGeometry(QRect(20, 260, 47, 13))
        self.StockInput = QLineEdit(AddProductWindow)
        self.StockInput.setObjectName(u"StockInput")
        self.StockInput.setGeometry(QRect(20, 280, 120, 20))
        self.AddProductButton = QPushButton(AddProductWindow)
        self.AddProductButton.setObjectName(u"AddProductButton")
        self.AddProductButton.setGeometry(QRect(120, 320, 75, 23))
        font1 = QFont()
        font1.setBold(True)
        font1.setWeight(75)
        self.AddProductButton.setFont(font1)
        self.AddProductButton.setAutoFillBackground(False)
        self.AddProductButton.setStyleSheet(u"QPushButton{\n"
" background-color:rgb(0, 214, 0);\n"
" color:rgb(255, 255, 255)\n"
"}")

        self.retranslateUi(AddProductWindow)

        QMetaObject.connectSlotsByName(AddProductWindow)
    # setupUi

    def retranslateUi(self, AddProductWindow):
        AddProductWindow.setWindowTitle(QCoreApplication.translate("AddProductWindow", u"A\u00f1adir Producto", None))
        self.WindowTitle.setText(QCoreApplication.translate("AddProductWindow", u"A\u00d1ADIR PRODUCTO", None))
        self.NameLabel.setText(QCoreApplication.translate("AddProductWindow", u"Nombre", None))
        self.DescriptionLabel.setText(QCoreApplication.translate("AddProductWindow", u"Descripci\u00f3n", None))
        self.PriceLabel.setText(QCoreApplication.translate("AddProductWindow", u"Precio", None))
        self.DateLabel.setText(QCoreApplication.translate("AddProductWindow", u"Fecha de Ingreso", None))
        self.StockLabel.setText(QCoreApplication.translate("AddProductWindow", u"Cantidad", None))
        self.AddProductButton.setText(QCoreApplication.translate("AddProductWindow", u"A\u00f1adir", None))
    # retranslateUi

