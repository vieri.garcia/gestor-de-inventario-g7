# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'app.window.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class AppWindown(object):
    def setupUi(self, AppWindown):
        if not AppWindown.objectName():
            AppWindown.setObjectName(u"AppWindown")
        AppWindown.resize(800, 600)
        self.centralwidget = QWidget(AppWindown)
        self.centralwidget.setObjectName(u"centralwidget")
        self.ButtonsFrame = QFrame(self.centralwidget)
        self.ButtonsFrame.setObjectName(u"ButtonsFrame")
        self.ButtonsFrame.setGeometry(QRect(20, 40, 451, 51))
        self.ButtonsFrame.setFrameShape(QFrame.StyledPanel)
        self.ButtonsFrame.setFrameShadow(QFrame.Raised)
        self.AddProductButton = QPushButton(self.ButtonsFrame)
        self.AddProductButton.setObjectName(u"AddProductButton")
        self.AddProductButton.setGeometry(QRect(20, 10, 121, 31))
        icon = QIcon()
        icon.addFile(u"./images/add_icon.png", QSize(), QIcon.Normal, QIcon.Off)
        self.AddProductButton.setIcon(icon)
        self.DeleteProduct = QPushButton(self.ButtonsFrame)
        self.DeleteProduct.setObjectName(u"DeleteProduct")
        self.DeleteProduct.setGeometry(QRect(160, 10, 121, 31))
        icon1 = QIcon()
        icon1.addFile(u"./images/delete_icon.png", QSize(), QIcon.Normal, QIcon.Off)
        self.DeleteProduct.setIcon(icon1)
        self.SearcherFrame = QFrame(self.centralwidget)
        self.SearcherFrame.setObjectName(u"SearcherFrame")
        self.SearcherFrame.setGeometry(QRect(20, 110, 751, 61))
        self.SearcherFrame.setFrameShape(QFrame.StyledPanel)
        self.SearcherFrame.setFrameShadow(QFrame.Raised)
        self.SearchLabel = QLabel(self.SearcherFrame)
        self.SearchLabel.setObjectName(u"SearchLabel")
        self.SearchLabel.setGeometry(QRect(10, 20, 61, 23))
        self.SearchText = QTextBrowser(self.SearcherFrame)
        self.SearchText.setObjectName(u"SearchText")
        self.SearchText.setGeometry(QRect(190, 20, 431, 23))
        self.SearchCriteria = QComboBox(self.SearcherFrame)
        self.SearchCriteria.setObjectName(u"SearchCriteria")
        self.SearchCriteria.setGeometry(QRect(70, 20, 111, 23))
        self.SearchButton = QPushButton(self.SearcherFrame)
        self.SearchButton.setObjectName(u"SearchButton")
        self.SearchButton.setGeometry(QRect(630, 20, 81, 23))
        icon2 = QIcon()
        icon2.addFile(u"./images/search_icon.png", QSize(), QIcon.Normal, QIcon.Off)
        self.SearchButton.setIcon(icon2)
        self.ProductsTable = QTableWidget(self.centralwidget)
        self.ProductsTable.setObjectName(u"ProductsTable")
        self.ProductsTable.setGeometry(QRect(20, 190, 751, 311))
        AppWindown.setCentralWidget(self.centralwidget)
        self.menubar = QMenuBar(AppWindown)
        self.menubar.setObjectName(u"menubar")
        self.menubar.setGeometry(QRect(0, 0, 800, 21))
        AppWindown.setMenuBar(self.menubar)
        self.statusbar = QStatusBar(AppWindown)
        self.statusbar.setObjectName(u"statusbar")
        AppWindown.setStatusBar(self.statusbar)

        self.retranslateUi(AppWindown)

        QMetaObject.connectSlotsByName(AppWindown)
    # setupUi

    def retranslateUi(self, AppWindown):
        AppWindown.setWindowTitle(QCoreApplication.translate("AppWindown", u"MainWindow", None))
        self.AddProductButton.setText(QCoreApplication.translate("AppWindown", u" A\u00f1adir Producto", None))
        self.DeleteProduct.setText(QCoreApplication.translate("AppWindown", u" Eliminar Producto", None))
        self.SearchLabel.setText(QCoreApplication.translate("AppWindown", u"Buscar por: ", None))
        self.SearchButton.setText(QCoreApplication.translate("AppWindown", u"Buscar", None))
    # retranslateUi

